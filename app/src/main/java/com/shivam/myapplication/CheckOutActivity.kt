package com.shivam.myapplication

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.shivam.myapplication.databinding.ActivityCheckOutBinding


class CheckOutActivity : AppCompatActivity() {
    lateinit var binding: ActivityCheckOutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckOutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM;
        supportActionBar?.customView = binding.toolbar;
        supportActionBar?.elevation = 0f

        binding.apply {
            rvServices.layoutManager = LinearLayoutManager(this@CheckOutActivity)
            rvServices.adapter = CheckOutAdapter()

            materialButton2.setOnClickListener {
                startActivity(Intent(this@CheckOutActivity, RatingActivity::class.java))
            }

            button.setOnClickListener {
                startActivity(Intent(this@CheckOutActivity, OrderTrackingActivity::class.java))
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        return true
    }
}