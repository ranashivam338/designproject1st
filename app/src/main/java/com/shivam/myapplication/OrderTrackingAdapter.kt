package com.shivam.myapplication

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shivam.myapplication.databinding.OrderTrackingAdapterBinding

class OrderTrackingAdapter : RecyclerView.Adapter<OrderTrackingAdapter.ItemHolder>() {
    class ItemHolder(val binding: OrderTrackingAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        var binding =
            OrderTrackingAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemHolder(binding)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        with(holder) {
            if (position == 0) {
                binding.statusDivider.visibility = View.GONE
                binding.statusIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_baseline_radio_button_checked_24))
            } else {
                binding.statusDivider.visibility = View.VISIBLE
            }

        }

    }

    override fun getItemCount(): Int {
        return 5
    }
}