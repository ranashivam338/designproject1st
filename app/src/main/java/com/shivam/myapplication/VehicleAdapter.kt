package com.shivam.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shivam.myapplication.databinding.VehicleAdapterBinding

class VehicleAdapter(): RecyclerView.Adapter<VehicleAdapter.ItemHolder>() {
    class ItemHolder(val binding: VehicleAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        var binding =
            VehicleAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        with(holder) {


        }
    }

    override fun getItemCount(): Int {
        return 5
    }
}