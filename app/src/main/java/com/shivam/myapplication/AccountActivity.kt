package com.shivam.myapplication

import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.shivam.myapplication.databinding.ActivityAccountBinding


class AccountActivity : AppCompatActivity() {

    lateinit var binding: ActivityAccountBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM;
        supportActionBar?.customView = findViewById(R.id.toolbar);
        supportActionBar?.elevation = 0f

        val someFragment: Fragment = FragmentViewPager()
        supportFragmentManager.beginTransaction().replace(R.id.fragmentContainerView, someFragment)
            .addToBackStack(null).commit()


    }
}