package com.shivam.myapplication

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class AccountViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                AboutFragment()
            }
            1 -> {
                BookingFragment()
            }
            else -> {
                MyVehicleFragment()
            }

        }

    }
}