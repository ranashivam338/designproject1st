package com.shivam.myapplication

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.shivam.myapplication.databinding.FragmentViewPagerBinding

class FragmentViewPager : Fragment(R.layout.fragment_view_pager){

    var _binding: FragmentViewPagerBinding? = null
    val binding get() = _binding


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentViewPagerBinding.bind(view)

        binding?.apply {
            viewPager.adapter = AccountViewPagerAdapter(this@FragmentViewPager)
            viewPager.post {


                TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                    tab.apply {
                        when (position) {
                            0 -> text = "About Me"
                            1 -> text = "My Bookings"
                            2 -> text = "My Vehicles"
                        }
                    }
                }.attach()
            }
        }
    }
}