package com.shivam.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shivam.myapplication.databinding.CheckoutAdapterBinding
import com.shivam.myapplication.databinding.ServiceHistoryAdapterBinding

class CheckOutAdapter : RecyclerView.Adapter<CheckOutAdapter.ItemHolder>() {
    class ItemHolder(val binding: CheckoutAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {



    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        var binding =
            CheckoutAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 5
    }
}