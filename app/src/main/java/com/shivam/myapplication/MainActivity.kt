package com.shivam.myapplication

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.shivam.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            mainCard.setBackgroundResource(R.drawable.cardbg)
            serviceHistoryItem.layoutManager = LinearLayoutManager(this@MainActivity)
            serviceHistoryItem.adapter = ServiceHistoryAdapter()

            downBtn.setOnClickListener {
                startActivity(Intent(this@MainActivity, CheckOutActivity::class.java))
            }
        }

    }
}