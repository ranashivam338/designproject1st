package com.shivam.myapplication

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class OrderTrackingActivity : AppCompatActivity() {
    @SuppressLint("CutPasteId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_tracking)

        findViewById<RecyclerView>(R.id.rvTracking).layoutManager = LinearLayoutManager(this)
        findViewById<RecyclerView>(R.id.rvTracking).adapter = OrderTrackingAdapter()

        findViewById<Button>(R.id.button3).setOnClickListener {
            startActivity(Intent(this, AccountActivity::class.java))
        }

        findViewById<Button>(R.id.button2).setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }
    }
}