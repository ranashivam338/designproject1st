package com.shivam.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shivam.myapplication.databinding.ServiceHistoryAdapterBinding

class ServiceHistoryAdapter : RecyclerView.Adapter<ServiceHistoryAdapter.ItemHolder>() {
    class ItemHolder(val binding: ServiceHistoryAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        var binding =
            ServiceHistoryAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        with(holder) {

            if (position == 0) {
                binding.materialButton.visibility = View.VISIBLE
            } else {
                binding.materialButton.visibility = View.GONE
            }

            Glide.with(itemView).asGif().load(R.drawable.ripple).override(24, 24)
                .into(binding.ripple)

            Glide.with(itemView).asGif().load(R.drawable.ripple).override(24, 24)
                .into(binding.ripple2)
        }
    }

    override fun getItemCount(): Int {
        return 5
    }
}