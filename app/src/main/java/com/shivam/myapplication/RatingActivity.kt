package com.shivam.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView

class RatingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rating_activity)
        findViewById<CardView>(R.id.mainCard).setBackgroundResource(R.drawable.cardbg)
    }
}